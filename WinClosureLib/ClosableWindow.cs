﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WinClosureLib
{
    public class ClosableWindow : Window
    {
        public static readonly DependencyProperty WinCloseEventProperty =
            DependencyProperty.RegisterAttached("WinCloseEvent", typeof(List<Action>), typeof(ClosableWindow), new PropertyMetadata(null, WinCloseEventChanged));
        public static void SetWinCloseEvent(ClosableWindow win, List<Action> handler)
        {
            win.SetValue(WinCloseEventProperty, handler);
        }
        public static List<Action> GetWinCloseEvent(ClosableWindow win)
        {
            return (List<Action>)win.GetValue(WinCloseEventProperty);
        }

        private static void WinCloseEventChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is ClosableWindow win && win != null && e.NewValue is List<Action> handler && handler != null)
            {
                handler.Clear();
                handler.Add( () => win.Close());
            }
        }
    }
}
