﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MVVM.ViewModel;

namespace WinClosure
{
    class MainViewModel : ViewModelBase
    {

        private DelegateCommand closeMe;
        public ICommand CloseMe
        {
            get
            {
                if (closeMe == null)
                {
                    closeMe = new DelegateCommand(_ => FireCloseMe());
                }
                return closeMe;
            }
        }

        private void FireCloseMe() 
        {
            foreach (Action item in CloseEvtProp)
            {
                item();
            }
        }

        private List<Action> closeEvtProp = new List<Action>();

        public List<Action> CloseEvtProp
        {
            get { return closeEvtProp; }
            set {
                closeEvtProp = value;
                OnPropertyChanged(); }
        }

    }
}
